package org.ffernanl;

public class FizzBuzz {

    public FizzBuzz() {
        printLines();
    }

    public void printLines() {
        for (int i = 0; i < 100; i++) {
            if (checkIfIsMultipleOfThreeAndFive(i))
                System.out.println("FizzBuzz");
            else if (checkIfIsMultipleOfFive(i))
                System.out.println("Buzz");
            else if (checkIfIsMultipleOfThree(i))
                System.out.println("Fizz");
            else System.out.println(i);
        }
    }

    public boolean checkIfIsMultipleOfThree(int num) {
        return num % 3 == 0;
    }

    public boolean checkIfIsMultipleOfFive(int num) {
        return num % 5 == 0;
    }

    public boolean checkIfIsMultipleOfThreeAndFive(int num) {
        return checkIfIsMultipleOfThree(num) && checkIfIsMultipleOfFive(num);
    }
}
